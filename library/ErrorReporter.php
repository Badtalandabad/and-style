<?php

/**
 * Class ErrorReporter
 *
 * Gathers error reporting messages
 */
class ErrorReporter
{
    /**
     * Warnings. Messages of the different issues.
     * They will be shown to the user if error reporting is on
     *
     * @var array
     */
    private static $_warnings = array();

    /**
     * Message of the critical error, which stop the system
     *
     * @var string
     */
    private static $_errorMessage;

    /**
     * Adds warning message to its storage
     *
     * @param   string  $message    Warning text
     *
     * @return  void
     */
    public static function addWarning( $message )
    {
        self::$_warnings[] = $message;
    }

    /**
     * Sets error message
     *
     * @param   string  $message    Error message text
     *
     * @return  void
     */
    public static function setError( $message )
    {
        self::$_errorMessage = $message;
    }

    /**
     * Returns warnings array
     *
     * @return  array
     */
    public static function getWarnings()
    {
        return self::$_warnings;
    }

    /**
     * Returns critical error object
     *
     * @return  string|bool
     */
    public static function getErrorMessage()
    {
        if ( empty( self::$_errorMessage ) ) {
            return false;
        }
        return self::$_errorMessage;
    }

}