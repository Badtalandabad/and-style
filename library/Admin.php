<?php

/**
 * Class Admin
 */
class Admin extends Application
{
    const MODULE_QUESTIONS = 'questions';
    const MODULE_REQUESTS = 'requests';

    /**
     * Renders template
     *
     * @param   string  $templateName   Template name
     * @param   array   $params         Parameters
     *
     * @return  string
     */
    private function _render( $templateName, $params = [] )
    {
        ob_start();

        require HTML_TEMPLATE_PATH . '/_' . $templateName . '.phtml';
        $content = ob_get_contents();

        ob_end_clean();

        return $content;
    }

    /**
     * Checks presence of the error and warnings in the session variables and
     * registers those in the error reporter
     */
    private function _registerErrorsFromSession()
    {
        $systemWarning = \Requester::getSessionVariable( 'systemWarning' );
        $systemError = \Requester::getSessionVariable( 'systemError' );
        if ( $systemWarning ) {
            \ErrorReporter::addWarning( $systemWarning );
            \Requester::deleteSessionVariable( 'systemWarning' );
        }
        if ( $systemError ) {
            \ErrorReporter::setError( $systemError );
            \Requester::deleteSessionVariable( 'systemError' );
        }
    }

    /**
     * Admin constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_registerErrorsFromSession();
    }

    /**
     * Tasks in requests module
     *  - default: display all requests
     *
     * @param   string  $task   Task
     *
     * @return  null|string
     */
    public function requestsTask( $task )
    {
        $content = null;
        $itemsPerPage = \Configuration::getParameter( 'adminRequestsPerPage' );

        switch ( $task ) {
            default:
                $page = (int)Requester::get( 'page' ) < 1 ? 1 : (int)Requester::get( 'page' );

                $offset = $itemsPerPage * ( $page - 1 );
                $requestsSet = $this->_model->loadRequests( $itemsPerPage, $offset );
                $requests = $requestsSet->requests;
                $pagesCount = ceil( $requestsSet->count / $itemsPerPage );
                
                $params = [
                    'requests' => $requests,
                    'pages_count' => $pagesCount ? $pagesCount : 1,
                    'page' => $page,
                ];
                $content = $this->_render( self::MODULE_REQUESTS, $params );
        }
        return $content;
    }

    /**
     * Tasks in questions module
     *  - edit: show "edit question" form
     *  - add: show "add question" form
     *  - save: save changed question
     *  - delete: remove question from the database
     *
     * @param   string  $task   Task
     *
     * @return  null|string
     *
     * @throws  AdminException
     */
    public function questionsTask( $task )
    {
        $content = null;

        $params = [];
        switch ( $task ) {
            case 'edit':
                $questionId = ctype_digit( Requester::get( 'id' ) ) ? Requester::get( 'id' ) : '';
                if ( !$questionId || !$question = $this->_model->loadQuestion( $questionId ) ) {
                    $errorMessage = 'Записи с таким id не существует';
                    throw (new AdminException( $errorMessage ))->setUrlToRedirect(
                        Router::route( [ 'task' => self::MODULE_QUESTIONS ] )
                    );
                }
                $params['id'] = $questionId;
                $params['question'] = $question->question;
                $params['description'] = $question->description;
                $params['number'] = $question->order_number;
                $params['decision_yes'] = $question->decision_yes;
                $params['decision_no'] = $question->decision_no;
            case 'add':
                if ( Requester::getSessionVariable( 'isRestoreNeeded' ) ) {
                    $restored = Requester::getSessionVariable( 'restoredData' );
                    $params = !empty( $restored['params'] ) && is_array( $restored['params'] )
                        ? array_merge( $params, $restored['params'] )
                        : $params;
                    Requester::deleteSessionVariable( 'isRestoreNeeded' );
                    Requester::deleteSessionVariable( 'restoredData' );
                }
                $params['csrf_token'] = $this->_getCsrfToken();
                Requester::setSessionVariable( 'csrfToken', $params['csrf_token'] );
                $content = $this->_render( 'edit_question', $params );
                break;
            case 'delete':
                $questionId = Requester::get( 'id' );
                if ( $questionId ) {
                    $this->_model->deleteQuestion( $questionId );
                }
                break;
            case 'save':
                $question    = trim( Requester::get( 'question' ) );
                $description = trim( Requester::get( 'description' ) );
                $number      = trim( Requester::get( 'number' ) );
                $decisionYes = trim( Requester::get( 'decision_yes' ) );
                $decisionNo  = trim( Requester::get( 'decision_no' ) );
                $id = Requester::get( 'id' );

                $errorMessage = '';
                if ( !$question || !$number || !$decisionYes || !$decisionNo ) {
                    $errorMessage = 'Не все поля заполнены!';
                }
                if ( isset($id) && !ctype_digit( $id ) ) {
                    $errorMessage = 'Некорректный ID';
                }
                if ( !ctype_digit( $number ) ) {
                    $errorMessage = 'Номер должен быть целым числом';
                }
                if ( $this->_model->isQuestionNumberUsing( $id, $number ) ) {
                    $errorMessage = 'Вопрос с таким номером уже существует!';
                }
                $csrfToken = Requester::getSessionVariable( 'csrfToken' );
                if ( !$csrfToken || $csrfToken != Requester::get( 'csrf_token' ) ) {
                    $errorMessage = 'Сессия истекла';
                }

                if ( !$errorMessage ) {
                    $result = $this->_model->saveQuestion($question, $description, 
                        $number, $decisionYes, $decisionNo, $id);
                    if ( !$result ) {
                        $errorMessage = 'Ошибка базы данных';
                    }
                }

                if ( $errorMessage ) {
                    if ( $id ) {
                        $urlParams = [ 'task' => self::MODULE_QUESTIONS . '.edit', 'id' => $id, ];
                    } else {
                        $urlParams = ['task' => self::MODULE_QUESTIONS . '.add'];
                    }
                    $restoredData = [ 'params' => [
                        'question' => $question,
                        'description' => $description,
                        'number' => $number,
                        'decision_yes' => $decisionYes,
                        'decision_no' => $decisionNo,
                        'id' => $id,
                    ]];
                    \Requester::setSessionVariable( 'restoredData', $restoredData );
                    \Requester::setSessionVariable( 'isRestoreNeeded', true );
                    throw (new AdminException( $errorMessage ))->setUrlToRedirect( Router::route( $urlParams ) );
                }
                break;
            default:
                $questions = $this->_model->loadQuestions();
                $content = $this->_render( self::MODULE_QUESTIONS, [ 'questions' => $questions ] );
        }

        return $content;
    }
    
    /**
     * Run the application
     */
    public function go()
    {
        $command = explode( '.', \Requester::get( 'task', self::MODULE_REQUESTS ) );
        $module = $command[0];
        $task = empty( $command[1] ) ? '' : $command[1];

        $content = null;

        try {
            switch ($module) {
                case self::MODULE_REQUESTS:
                    $content = $this->requestsTask($task);
                    break;
                case self::MODULE_QUESTIONS:
                    $content = $this->questionsTask($task);
                    break;
            }
        } catch (AdminException $e) {
            $url = $e->getUrlToRedirect() ? $e->getUrlToRedirect() : [ 'task' => $module, ];
            Requester::setSessionVariable( 'systemError', $e->getMessage() );
            Router::redirect( $url );
        } catch (Exception $e) {
            Requester::setSessionVariable( 'systemError', 'Внутренняя ошибка сервера' );
            Router::redirect( [ 'task' => $module, ] );
        }

        if ( is_null( $content ) ) {
            Router::redirect( [ 'task' => $module, ] );
        }

        require HTML_TEMPLATE_PATH . '/index.phtml';
    }

}