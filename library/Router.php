<?php

/**
 * Class Router
 */
class Router
{
    /**
     * Returns link to the certain task
     *
     * @param   array   $params Get parameters for the describing what to do
     *
     * @return  string
     */
    public static function route( $params = array() )
    {
        $uri = explode( '/', $_SERVER['REQUEST_URI'] );
        array_pop( $uri );
        $uri = implode( '/', $uri ) . '/index.php';

        $path = 'http://' . $_SERVER['HTTP_HOST'] . $uri;

        if ( empty( $params['task'] ) ) {
            return false;
        } else {
            $task = $params['task'];
        }

        $path .= '?task=' . urldecode( $task );
        foreach ( $params as $name => $value ) {
            if ( $name == 'task' ) {
                continue;
            }
            $path .= '&' . urldecode( $name ) . '=' . urldecode( $value );
        }

        return $path;
    }

    /**
     * Redirect to the url with certain task
     *
     * @param   array|string    $destination    Parameters or URL
     *
     * @return  void
     */
    public static function redirect( $destination = [] )
    {
        if ( is_array( $destination ) ) {
            $destination = self::route( $destination );
        }
        header( 'Location: ' . $destination );

        exit;
    }

}