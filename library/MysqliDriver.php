<?php

/**
 * Database Mysqli driver
 */
class MysqliDriver
{
    /**
     * Database object
     *
     * @var Db
     */
    protected $_db;

    /**
     * Driver name
     *
     * @var string
     */
    protected $_name = 'mysqli';

    /**
     * Database hostname.
     *
     * @var string
     */
    protected $_dbHost;

    /**
     * Database name.
     *
     * @var string
     */
    protected $_dbName;

    /**
     * Database username.
     *
     * @var string
     */
    protected $_dbUser;

    /**
     * Database password.
     *
     * @var string
     */
    protected $_dbPassword;

    /**
     * Database charset
     *
     * @var string
     */
    protected $_dbCharset;

    /**
     * Quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @var string
     */
    protected $_quoteForNames = '`';

    /**
     * Quote symbol. Which using in quote() and q() methods
     *
     * @var string
     */
    protected $_quote = '\'';

    /**
     * MySQLi object
     *
     * @var \mysqli
     */
    private $_mysqli;

    /**
     * Sets database object
     *
     * @param   Db  $db Database object
     */
    protected function setDb($db)
    {
        $this->_db = $db;
    }

    /**
     * Returns database object
     *
     * @return  Db
     */
    public function getDb()
    {
        return $this->_db;
    }

    /**
     * Returns driver name
     *
     * @return  string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Sets driver name
     *
     * @param   string  $name   Driver name
     *
     * @return  $this
     */
    public function setName( $name )
    {
        $this->_name = $name;

        return $this;
    }

    /**
     * Sets database host
     *
     * @param   string  $dbHost Database host
     *
     * @return  $this
     */
    public function setDbHost( $dbHost )
    {
        $this->_dbHost = $dbHost;

        return $this;
    }

    /**
     * Returns database host
     *
     * @return string
     */
    public function getDbHost()
    {
        return $this->_dbHost;
    }

    /**
     * Sets database name
     *
     * @param   string  $dbName Database name
     *
     * @return  $this
     */
    public function setDbName( $dbName )
    {
        $this->_dbName = $dbName;

        return $this;
    }

    /**
     * Returns database name
     *
     * @return string
     */
    public function getDbName()
    {
        return $this->_dbName;
    }

    /**
     * Sets database username
     *
     * @param   string  $dbUser Database user
     *
     * @return  $this
     */
    public function setDbUser( $dbUser )
    {
        $this->_dbUser = $dbUser;

        return $this;
    }

    /**
     * Returns database username
     *
     * @return string
     */
    public function getDbUser()
    {
        return $this->_dbUser;
    }

    /**
     * Sets database password
     *
     * @param   string  $dbPassword Database user password
     *
     * @return  $this
     */
    public function setDbPassword( $dbPassword )
    {
        $this->_dbPassword = $dbPassword;

        return $this;
    }

    /**
     * Returns database password
     *
     * @return  string
     */
    public function getDbPassword()
    {
        return $this->_dbPassword;
    }

    /**
     * Sets database charset
     *
     * @param   string  $dbCharset  Database charset
     *
     * @return  $this
     */
    public function setDbCharset( $dbCharset )
    {
        $this->_dbCharset = $dbCharset;

        return $this;
    }

    /**
     * Returns database charset
     *
     * @return string
     */
    public function getDbCharset()
    {
        return $this->_dbCharset;
    }


    /**
     * Sets quote symbol. Which using in quote() and q() methods
     *
     * @param   string  $quote  Quote symbol
     *
     * @return  $this
     */
    public function setQuote( $quote )
    {
        $this->_quote = $quote;

        return $this;
    }

    /**
     * Returns quote symbol. Which using in quote() and q() methods
     *
     * @return  string
     */
    public function getQuote()
    {
        return $this->_quote;
    }

    /**
     * Sets quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @param   string  $quoteForNames  Quote symbol
     *
     * @return  $this
     */
    public function setQuoteForNames( $quoteForNames )
    {
        $this->_quoteForNames = $quoteForNames;

        return $this;
    }

    /**
     * Returns quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @return  string
     */
    public function getQuoteForNames()
    {
        return $this->_quoteForNames;
    }

    /**
     * Constructor. Connects to the database
     *
     * @param   Db  $db         Database object
     * @param   string          $dbHost     Database host
     * @param   string          $dbUser     Database user
     * @param   string          $dbPassword Database password
     * @param   string          $dbCharset  Database charset
     * @param   string          $dbName     Database name
     *
     * @throws  \Exception  If MySQL connection failed
     */
    public function __construct( $db, $dbHost, $dbUser, $dbPassword, $dbCharset, $dbName )
    {
        $this->setDb( $db );

        $this->setDbHost( $dbHost );
        $this->setDbUser( $dbUser );
        $this->setDbPassword( $dbPassword );
        $this->setDbCharset( $dbCharset );
        $this->setDbName( $dbName );
    }

    /**
     * Quotes and optionally escapes a string to
     * database requirements for insertion into the database
     *
     * @param   string  $text   Text to quote
     * @param   bool    $escape To escape or not
     *
     * @return  string
     */
    public function quote( $text, $escape = true )
    {
        if ( $escape ) {
            $text = $this->escapeValue( $text );
        }

        $quote = $this->getQuote();

        $quotedText = $quote . $text . $quote;

        return $quotedText;
    }

    /**
     * Wraps an SQL statement identifier name such as column,
     * table or database names in quotes
     *
     * @param   string  $name   Text to quote
     * @param   bool    $escape To escape or not
     *
     * @return  string
     */
    public function quoteName( $name, $escape = true )
    {
        if ( $escape ) {
            $name = $this->escapeName( $name );
        }

        $quote = $this->getQuoteForNames();

        $quotedName = $quote . $name . $quote;

        return $quotedName;
    }

    /**
     * Sets mysqli object
     *
     * @param   \mysqli $mysqli Mysqli object
     *
     * @return  $this
     */
    public function setMysqli( $mysqli )
    {
        $this->_mysqli = $mysqli;

        return $this;
    }

    /**
     * Returns mysqli object
     *
     * @return \mysqli
     */
    public function getMysqli()
    {
        return $this->_mysqli;
    }

    /**
     * Connects to the database
     *
     * @throws  \Exception  If Mysql connection failed
     *
     * @return  bool
     */
    public function connect()
    {
        $mysqli = new \mysqli(
            $this->getDbHost(),
            $this->getDbUser(),
            $this->getDbPassword(),
            $this->getDbName()
        );

        $this->setMysqli( $mysqli );

        if ( $mysqli->connect_errno ) {
            $exceptionMessage = 'MySQL connection failure(' . $mysqli->connect_errno
                . ") " . $mysqli->connect_error;
            throw new \Exception( $exceptionMessage );
        }

        $this->getMysqli()
            ->set_charset( $this->getDbCharset() );

        return true;
    }

    /**
     * Executes sql query
     *
     * @param   string  $query  Query to execute
     *
     * @return  bool|\mysqli_result
     */
    public function execute( $query )
    {
        return $this->getMysqli()
            ->query( $query );
    }

    /**
     * Escapes text for usage in an SQL statement.
     * Uses mysqli_real_escape_string.
     *
     * @param   string  $text   Text to escape
     *
     * @return  string
     */
    public function escapeValue( $text )
    {
        $escapedText = $this->getMysqli()
            ->real_escape_string( $text );

        return $escapedText;
    }

    /**
     * Escapes column name for usage in an SQL statement.
     * Uses mysqli_real_escape_string.
     *
     * @param   string  $name   Name to escape
     *
     * @return  string
     */
    public function escapeName( $name )
    {
        return $this->escapeValue( $name );
    }

}