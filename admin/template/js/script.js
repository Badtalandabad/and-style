$(function () {
    $('.b-question-remove-link').click(function () {
        var message = 'Вы уверены, что хотите удалить вопрос №' + $(this).attr('data-number') + '?';
        if (!confirm(message)) {
            return false;
        }
    });
});