$(function () {
    $('.b-order-form').submit(function () {
        var form = $(this);

        var emailInput = form.find('[name="email"]');
        var phoneInput = form.find('[name="phone"]');
        var commentInput = form.find('[name="comment"]');
        if ((!emailInput.val() && !phoneInput.val()) || !commentInput.val()) {
            if ($('.b-form-message').css('display') == 'block') {
                return false;
            }
            $('.b-form-message').text('Чтобы отправить заявку нужно заполнить комментарий и один из контактов')
                .fadeIn().delay(3000).fadeOut();

            return false;
        }

        $.post('/?ajax=1', $(this).serialize())
        .always(function () {
            form.fadeOut();
        }).done(function (data) {
            $('.b-form-success').delay(600).fadeIn();
        }).fail(function (data) {
            $('.b-form-fail').delay(600).fadeIn();
        });
        return false;
    });
});